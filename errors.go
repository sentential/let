// Copyright (c) 2015 Sentential Pty Ltd
// All rights reserved.
//
// Released under zlib/png licence (see LICENCE)

package let

import (
	"encoding/json"
	"fmt"
	"log"
)

// This is a common error type
type internalErrorCouchDB struct {
	// CouchDB error string
	Error string `json:"error"`

	// CouchDB reason string
	Reason string `json:"reason"`
}

// Internal function to check if the internal message comes back as something akin to `{"ok":"value"}`
func couchIsOk(status int, body *string) (ok bool) {

	// This is only to be used here.
	type internalOK struct {
		Ok bool `json:"ok"`
	}

	ok = false
	if status == 200 || status == 201 || status == 202 {
		var Ok internalOK
		err := json.Unmarshal([]byte(*body), &Ok)
		if err == nil {
			ok = Ok.Ok
		}
	}
	return
}

// Database error
func parseErrorCouchDB(status int, body *string) (err error) {

	var iErr internalErrorCouchDB
	err = json.Unmarshal([]byte(*body), &iErr)
	if err != nil {
		log.Fatal("Unable to decode error -- non-error or something has changed!")
	}
	return fmt.Errorf("---\nStatus Code: %d\nError: '%s'\nReason: '%s'\n---\n", status, iErr.Error, iErr.Reason)
}
