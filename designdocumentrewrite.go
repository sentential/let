// Copyright (c) 2015 Sentential Pty Ltd
// All rights reserved.
//
// Released under zlib/png licence (see LICENCE)

package let

// DesignDocumentRewrite represents a rewrite rule from a design document.
type DesignDocumentRewrite struct {
	From   string `json:"from"`
	To     string `json:"to"`
	Method string `json:"method"`
	Query  string `json:"query"`
}
