package main

import (
	"bitbucket.org/sentential/let"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
	"sync/atomic"
	"time"
)

// 50 documents
var bulkDocumentCount int32 = 50

// Test document structure
type TestDoc struct {
	Str   string  `json:"String"`
	Float float64 `json:"Float"`
	Value TestDocValue
}

// Test document filler
type TestDocValue struct {
	Test string
}

func Usage() {
	fmt.Println("Usage:")
	fmt.Printf("%s --uri <uri>", os.Args[0])
	fmt.Println("  --username <username>")
	fmt.Println("  --password <password>")
	fmt.Println("  --db <db>")
	fmt.Println("  --cert <cert>")
	fmt.Println("  --key <key>")
	fmt.Println("  --ca <caFile>")
}

func GetDocAsync(db *let.Database, uuid string, countFound *int32, countTotal *int32) {
	doc, err := db.Get(uuid, "")
	if err != nil || doc == nil {
		atomic.AddInt32(countTotal, 1)
		return
	}
	atomic.AddInt32(countFound, 1)
	atomic.AddInt32(countTotal, 1)
}

func main() {

	// Use stdout, but keep log live for the main package.
	log.SetOutput(os.Stdout)

	var username *string = flag.String(
		"username",
		"",
		"Username")

	var password *string = flag.String(
		"password",
		"",
		"Password for the given username")

	var uri *string = flag.String(
		"uri",
		"http://127.0.0.1:5984",
		"Host (e.g. http://127.0.0.1:5984 or https://my_proxy_virtualhosting_magic/some_prefix)")

	var dbName *string = flag.String(
		"db",
		"twiggly",
		"Database to get information about.")

	var certFile *string = flag.String(
		"cert",
		"",
		"Path to certificate (for SSL); blank will load non-SSL.")

	var keyFile *string = flag.String(
		"key",
		"",
		"Path to key file (for SSL); blank will load non-SSL.")

	var caFile *string = flag.String(
		"ca",
		"",
		"Path to key file (for SSL); blank will load non-SSL.")

	flag.Parse()

	// Create a server
	server, err := let.ConnectTLS(*uri, *certFile, *keyFile, *caFile)

	// Authenticate
	server.SetCookieAuth(*username, *password)

	// Get the session information; it should report a 'null' username.

	if !server.IsAuthenticated() {
		fmt.Println("Not authenticated; cannot continue.")
		return
	}

	session, err := server.GetSession()
	fmt.Printf("Username: '%s'\n", session.UserData.Name)

	// Test server information
	info, err := server.Info()
	if err == nil {
		fmt.Printf("CouchDB Version: %s\n", info.Version)
	}

	// Gets the database
	db, err := server.GetOrCreate(*dbName)

	// Inform (with any luck there's no error; if there is, something funny is happening or the database doesn't exist)
	if err != nil {
		fmt.Printf("Error getting database ('%s'):\n%s\n", *dbName, err)
		return // fatal
	}

	exitNuke := false

	// If the count is zero nuke it on close
	if db.Info.DocumentCount == 0 {
		exitNuke = true
	}

	// The name is exposed (compare to expected)
	fmt.Printf("Database name: '%s' (expected '%s')\n", db.Name, *dbName)

	// Create a new document
	doc := db.NewDocument()

	// Test document #1 content
	td := TestDoc{
		"Test",
		3.1415,
		TestDocValue{
			"Test",
		},
	}

	// Test document #2 content
	td2 := TestDoc{
		"Test 2",
		float64(4195835.0 / 3145727.0),
		TestDocValue{
			"Test2",
		},
	}

	fmt.Println("Created two test documents ...")

	// Write it
	doc_bytes, err := json.Marshal(&td)
	if err != nil {
		fmt.Printf("\tFailed to encode (#1): %s\n", err)
	}
	err = doc.Save(doc_bytes)
	if err != nil {
		fmt.Printf("\tFailed to save (#1): %s\n", err)
	}

	fmt.Println("First document stored...")

	doc_bytes2, err := json.Marshal(&td2)
	if err != nil {
		fmt.Printf("\tFailed to encode (#2): %s\n", err)
	}
	new_doc2, err := db.Save(doc_bytes2)
	if err != nil {
		fmt.Printf("\tFailed to save (#2): %s\n", err)
	}
	fmt.Println("Second document stored...")

	fmt.Println("Removing Revision from the document prior to removal...")
	doc.Revision = ""
	fmt.Println("Attempting Delete (should fail the first time)")

	if doc.Delete() {
		fmt.Println("  Removed first document. (Should have failed)")
	} else {
		fmt.Println("  Failed to remove first document. (Expected)")
		fmt.Println("  Attempting ForceDelete (auto-gathers latest revision to force it):")
		if doc.ForceDelete() {
			fmt.Println("    Removed (Expected)")
		} else {
			fmt.Println("    Failed to remove first document. (Unexpected)")
		}
	}

	fmt.Println("Testing SaveRaw:")
	if new_doc2.SaveRaw() {
		fmt.Println("Saved, new revision:", new_doc2.Revision)
	}

	if new_doc2.SaveRaw() {
		fmt.Println("Saved, new revision:", new_doc2.Revision)
	}

	premodrev := new_doc2.Revision

	fmt.Println("Manipulating the second document:")
	var td2m map[string]json.Token
	err = json.Unmarshal([]byte(new_doc2.RawDocument), &td2m)
	if err != nil {
		fmt.Println("Failed to unmarshal into generic/sane pseudo-interface.")
	}

	// Add key
	td2m["newKey"] = "newValue"

	td2m_bytes, err := json.Marshal(td2m)
	if err == nil {
		err = new_doc2.Save(td2m_bytes)
		if err == nil {
			fmt.Println("Saved #2.")
		} else {
			fmt.Println("Failed to save #2.")
		}
	} else {
		fmt.Println("Failed to re-marshal #2.")
	}

	fmt.Println("\n---")
	fmt.Println("Current document information in #2:")
	fmt.Println("---")
	fmt.Println(new_doc2.RawDocument)
	fmt.Println("---\n")

	new_doc3, err := new_doc2.Copy("")
	if err != nil {
		fmt.Printf("Failed to copy doc2->doc3: %s\n", err)
	}

	fmt.Println("\n---")
	fmt.Println("Document 3 contents")
	fmt.Println("---")
	fmt.Println(new_doc3.RawDocument)
	fmt.Println("---\n")

	new_doc4, err := new_doc2.CopyRevision(premodrev, "")
	if err != nil {
		fmt.Printf("Failed to copy doc2->doc4: %s\n", err)
	}

	fmt.Println("\n---")
	fmt.Println(`Document 4 contents (pre modification); note the lack of "newKey": "newValue"`)
	fmt.Println("---")
	fmt.Println(new_doc4.RawDocument)
	fmt.Println("---\n")

	uuids, err := server.GetUUIDs(int(bulkDocumentCount))
	if err != nil {
		log.Fatal("Unable to continue, failed to get UUIDs")
	}

	fmt.Println("Got UUIDs, generating empty/boring documents...")

	bulk := db.GetBulkDocumentObject()

	for _, v := range uuids {
		bulk.Documents = append(bulk.Documents, map[string]json.Token{"_id": v})
	}
	bulk.Save()

	// ready for things you probably won't like?
	var countFound int32 = 0
	var countTotal int32 = 0

	for _, v := range uuids {
		go GetDocAsync(db, v, &countFound, &countTotal)
	}

	for atomic.LoadInt32(&countTotal) < bulkDocumentCount {
		time.Sleep(250)
	}

	fmt.Printf("Found: %d/%d\n\n", atomic.LoadInt32(&countFound), atomic.LoadInt32(&countTotal))

	fmt.Println("Testing _all_docs")

	adPagination, err := db.AllDocuments()
	if err != nil {
		fmt.Printf("AllDocuments Pagination error encountered: %s\n", err)
	} else {
		fmt.Printf("Found: %d documents.\n", adPagination.Total)
		fmt.Printf("First document found:\n\tID:         %s\n\tRevision:   %s\n\n",
			adPagination.Rows[0].ID,
			adPagination.Rows[0].Value.(map[string]interface{})["rev"].(string))
	}

	if exitNuke {
		server.Delete(db)
		fmt.Println("Removing test database.")
	}

}
