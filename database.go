// Copyright (c) 2015 Sentential Pty Ltd
// All rights reserved.
//
// Released under zlib/png licence (see LICENCE)

package let

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"log"
)

// Database represents a CouchDB Database (tied to a Server object)
type Database struct {
	server *Server      // internal pointer to the server
	Name   string       // name
	dbRoot string       // Root (e.g. /_users for "_users")
	Info   DatabaseInfo // Information
}

// DatabaseInfoSizes is a json de/serialisation object representing _info["sizes"].
type DatabaseInfoSizes struct {
	File     uint64
	External uint64
	Active   uint64
}

// DatabaseInfoOther is a json de/serialisation object representing _info["other"]
type DatabaseInfoOther struct {
	Size uint64 `json:"data_size"`
}

// DatabaseInfo is a json de/serialisation object JSON reply from CouchDB 2.0 - as of 2015/09/21
type DatabaseInfo struct {
	Name                 string            `json:"db_name"`             // Name of the database
	UpdateSequenceNumber string            `json:"update_seq"`          // The current number of updates to the database.
	Sizes                DatabaseInfoSizes `json:"sizes"`               // Sizes (not described in the docs (yet))
	PurgeSequenceNumber  uint64            `json:"purge_seq"`           // The number of purge operations on the database.
	Other                DatabaseInfoOther `json:"other"`               // Other (not described in the docs (yet))
	DocumentDeleteCount  uint64            `json:"doc_del_count"`       // Number of deleted documents
	DocumentCount        uint64            `json:"doc_count"`           // A count of the documents in the specified database.
	SizeOnDisk           uint64            `json:"disk_size"`           // The length of the database file on disk; Views indexes are not included in the calculation.
	DiskFormatVersion    uint64            `json:"disk_format_version"` // The version of the physical format used for; the data when it is stored on disk.
	DataSize             uint64            `json:"data_size"`           // The number of bytes of live data inside the database file.
	CompactRunning       bool              `json:"compact_running"`     // Set to true if the database compaction routine is operating on this database.
	InstanceStartTime    string            `json:"instance_start_time"` // Timestamp of when the database was opened, expressed in microseconds since the epoch.
}

// UpdateInfo performs an update on the information (via GET request)
func (db *Database) UpdateInfo() (err error) {
	// Naturally this will fail on _users until OAuth/cookie/something is added.
	status, _, body, err := db.server.HTTPGet(db.dbRoot)

	// If there's an error
	if err != nil {
		return
	}

	// Non 200 codes are interesting.
	if status != 200 {
		// This data interesting for the purposes of crafting the error.
		return parseErrorCouchDB(status, &body)
	}

	// Decode into Info field
	err = json.Unmarshal([]byte(body), &db.Info)
	if err != nil {
		log.Println("Unable to decode session.")
	}
	return
}

// Get retreives a document (if it exists), or an error if it does not.
// _id and _rev must be provided (via id and rev); a blank _rev will trigger the latest.
func (db *Database) Get(id string, rev string) (doc *Document, err error) {
	if rev == "" {
		return db.GetLatest(id)
	}
	return db.GetByRevision(id, rev)
}

// GetLatest gets the latest version of a document (if it exists).
// _id must be provided (via id)
func (db *Database) GetLatest(id string) (doc *Document, err error) {
	doc = &Document{db, id, "", ""}
	err = doc.Load()
	if err != nil {
		doc = nil
	}
	return
}

// GetByRevision returns a document based on revision
func (db *Database) GetByRevision(id string, revision string) (doc *Document, err error) {
	doc = &Document{db, id, revision, ""}
	err = doc.Load()
	if err != nil {
		doc = nil
	}
	return
}

// GetDesignDocument returns a given design document (if it exists).
func (db *Database) GetDesignDocument(name string) (dd *DesignDocument, err error) {
	dd = &DesignDocument{
		db:       db,
		Name:     name,
		Path:     fmt.Sprintf("%s/_design/%s", db.Name, name),
		Filters:  make(map[string]string),
		Lists:    make(map[string]string),
		Rewrites: make([]*DesignDocumentRewrite, 0),
		Shows:    make(map[string]string),
		Updates:  make(map[string]string),
		Views:    make(map[string]*DesignDocumentView),
	}

	err = dd.Reload()
	if err != nil {
		dd = nil
	}
	return dd, err
}

// save performs the internal document save; it used by Document and Database objects.
func (db *Database) save(docID string, b []byte) error {
	if docID != "" {
		path := fmt.Sprintf("%s/%s", db.dbRoot, docID)
		status, _, _, err := db.server.HTTPPut(path, bytes.NewReader(b))

		// 201: // created
		// 202: // accepted but not written (fine)
		if status != 201 && status != 202 {
			// TODO: Neaten?
			switch status {
			case 400:
				return errors.New("400 - bad request during save")
			case 401:
				return errors.New("401 - unauthorised during save")
			case 404:
				return errors.New("404 - not found during save")
			case 409:
				return errors.New("409 - conflict during save")
			}
		}

		if err != nil {
			return err
		}
	}
	return nil
}

// Save triggers ad ocument save (based on serialised byte data).
func (db *Database) Save(b []byte) (doc *Document, err error) {

	var docID string
	changed := false
	var m map[string]json.Token
	err = json.Unmarshal(b, &m)
	if err != nil {
		return nil, fmt.Errorf("invalid unmarshall when checking byte data: %s", err)
	}

	// Unmarshall _id it out
	if v, ok := m["_id"]; ok {
		docID = v.(string)
	} else {
		uuid, err := db.server.GetUUID()
		if err != nil {
			return nil, fmt.Errorf("document does not have an id assigned or in binary data and attempts to get a uuid have failed: %s", err)
		}
		docID = uuid
		m["_id"] = uuid // store
		changed = true
	}

	// Check for a valid _rev
	if rev, ok := m["_rev"]; ok {
		if rev.(string) == "" {
			delete(m, "_rev")
			changed = true
		}
	}

	// remarshal if needed
	if changed {
		b, err = json.Marshal(m)
		if err != nil {
			return nil, fmt.Errorf("failed to remarshal data (database): %s", err)
		}
	}
	err = db.save(docID, b)
	if err != nil {
		return nil, err
	}
	return db.GetLatest(docID)

}

// GetBulkDocumentObject returns an empty BulkDocuments object pointing to the given database.
func (db *Database) GetBulkDocumentObject() *BulkDocuments {
	return &BulkDocuments{
		Database:     db,
		AllOrNothing: false,
		NewEdits:     true,
	}
}

// AllDocuments returns a list of all documents (as per _all_docs)
func (db *Database) AllDocuments() (*AllDocumentsPagination, error) {
	return db.AllDocumentsQuery(&PaginationQuery{})
}

// AllDocumentsQuery returns a list of all documents (as per _all_docs) based on the query object.
func (db *Database) AllDocumentsQuery(query *PaginationQuery) (*AllDocumentsPagination, error) {

	adp := &AllDocumentsPagination{
		db:    db,
		Query: query,
	}

	return adp, adp.Load()
}

// NewDocument creates an empty document bound to the database.
func (db *Database) NewDocument() *Document {
	return &Document{
		ID:          "",
		Revision:    "",
		RawDocument: "",
		database:    db,
	}
}
