package main

import (
	"bitbucket.org/sentential/let"
	"flag"
	"fmt"
	"log"
	"os"
)

func Usage() {
	fmt.Println("Usage:")
	fmt.Printf("%s --uri <uri>", os.Args[0])
	fmt.Println("  --username <username>")
	fmt.Println("  --password <password>")
	fmt.Println("  --db <db>")
	fmt.Println("  --design <design>")
	fmt.Println("  --view <view>")
	fmt.Println("  --viewkey <viewkey>")
	fmt.Println("  --cert <cert>")
	fmt.Println("  --key <key>")
	fmt.Println("  --ca <caFile>")
}

func main() {

	// Use stdout, but keep log live for the main package.
	log.SetOutput(os.Stdout)

	var username *string = flag.String(
		"username",
		"",
		"Username")

	var password *string = flag.String(
		"password",
		"",
		"Password for the given username")

	var uri *string = flag.String(
		"uri",
		"http://127.0.0.1:5984",
		"Host (e.g. http://127.0.0.1:5984 or https://my_proxy_virtualhosting_magic:4713/some_prefix)")

	var dbName *string = flag.String(
		"db",
		"twiggly",
		"Database to get information about.")

	var designdoc *string = flag.String(
		"design",
		"",
		"Design document name")

	var view *string = flag.String(
		"view",
		"",
		"View name")

	var viewkey *string = flag.String(
		"viewkey",
		"",
		"View key")

	var certFile *string = flag.String(
		"cert",
		"",
		"Path to certificate (for SSL); blank will load non-SSL.")

	var keyFile *string = flag.String(
		"key",
		"",
		"Path to key file (for SSL); blank will load non-SSL.")

	var caFile *string = flag.String(
		"ca",
		"",
		"Path to key file (for SSL); blank will load non-SSL.")

	flag.Parse()

	// Create a server
	server, err := let.ConnectTLS(*uri, *certFile, *keyFile, *caFile)

	// Authenticate
	server.SetCookieAuth(*username, *password)

	// Get the session information; it should report a 'null' username.
	session, err := server.GetSession()

	// Inform (with any luck there's no error; if there is, something funny is happening)
	if err != nil {
		fmt.Printf("Error getting session: %s\n", err)
	} else {
		fmt.Printf("Username: '%s'\n", session.UserData.Name)
	}

	// Gets the database
	db, err := server.Get(*dbName)

	// Inform (with any luck there's no error; if there is, something funny is happening or the database doesn't exist)
	if err != nil {
		fmt.Printf("Error getting database ('%s'):\n%s\n", *dbName, err)
		return
	}

	// The name is exposed (compare to expected)
	fmt.Printf("Database name: '%s' (expected '%s')\n", db.Info.Name, *dbName)

	dd, err := db.GetDesignDocument(*designdoc)
	if err != nil {
		fmt.Printf("Unable to get design document (%s)\n", *designdoc)
		return
	}

	fmt.Printf("Got design document...\n")

	dv, err := dd.GetView(*view)
	if err != nil {
		fmt.Printf("Unable to get view (_design/%s/_view/%s)\n", *designdoc, *view)
		return
	}

	fmt.Printf("Got design document view...\n")

	requested_rows := 12

	query := let.NewQuery()
	query.Key = *viewkey
	query.Limit = requested_rows
	query.Reduce = false
	query.Group = true

	page, err := dv.Query(query)
	if err != nil {
		fmt.Printf("Unable to get page from view (_design/%s/_view/%s)\n", *designdoc, *view)
		fmt.Println(err)
		return
	}

	fmt.Printf("Loaded (_design/%s/_view/%s)\n", *designdoc, *view)

	fmt.Printf("Got rows: %d (expected a maximum of %d); %d total reported\n", len(page.Rows), requested_rows, page.Total)

	// Do nothing special here; it's just to show it isn't broken.
	for k, v := range page.Rows {
		fmt.Println(k, v.Key, v.Value)
	}

}
