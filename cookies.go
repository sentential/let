// Copyright (c) 2015 Sentential Pty Ltd
// All rights reserved.
//
// Released under zlib/png licence (see LICENCE)

package let

import (
	"net/url"
)

// SetCookieAuth stores and attempts to use the username and password for authentication.
func (server *Server) SetCookieAuth(username string, password string) {
	server.username = username
	server.password = password
	server.DoCookieAuth()
}

// DoCookieAuth performs the authentication (based on stored credentials).
// Typically `SetCookieAuth` should be used directly; this function should
// only be used to handle authentication timeouts (verifiable using `IsAuthenticated`).
func (server *Server) DoCookieAuth() (err error) {
	if err == nil {
		resp, err := server.ClientHTTP.PostForm(server.ServerURL+"/_session",
			url.Values{
				"name":     {server.username},
				"password": {server.password},
			})
		if err == nil {
			status := resp.StatusCode
			if status == 200 {
				server.badAuth = false
			} else {
				server.badAuth = true
			}
		}
	}
	return
}
