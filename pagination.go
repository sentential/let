// Copyright (c) 2015 Sentential Pty Ltd
// All rights reserved.
//
// Released under zlib/png licence (see LICENCE)

package let

import (
	"fmt"
	"strings"
)

// Pagination is an interface which is designed to cover all common pagination
// requests.
type Pagination interface {
	Load() error     // Data is loaded.
	Next() error     // Skip moves up by limit and the next page is loaded.
	Previous() error // Skip moves down by limit (or to 0) and the previous page is loaded.
}

// PaginationJSON describes common variables
type PaginationJSON struct {
	Total int                      `json:"total_rows"` // total_rows field
	Skip  int                      `json:"offset"`     // offset field
	Rows  []*DesignDocumentViewRow `json:"rows"`       // rows field
}

// PaginationQuery represents the full range of options which may be queried.
// See: http://docs.couchdb.org/en/latest/api/ddoc/views.html
type PaginationQuery struct {
	Conflicts              bool
	Descending             bool
	EndKey                 string
	EndKeyDocID            string
	Group                  bool
	GroupLevel             int
	IncludeDocuments       bool
	Attachments            bool
	AttachmentEncodingInfo bool
	InclusiveEnd           bool
	Key                    string
	Keys                   []string
	Limit                  int
	Reduce                 bool
	Skip                   int
	Stale                  string
	StartKey               string
	StartKeyDocID          string
	UpdateSequence         bool
}

// NewQuery creates a new query (for pagination)
func NewQuery() *PaginationQuery {
	return &PaginationQuery{
		Conflicts:              false,
		Descending:             false,
		EndKey:                 "",
		EndKeyDocID:            "",
		Group:                  false,
		GroupLevel:             0,
		InclusiveEnd:           true,
		Attachments:            false,
		AttachmentEncodingInfo: false,
		Reduce:                 true,
	}
}

// String turns a pagination query into a string (using fmt.Sprintf)
func (pq *PaginationQuery) String() string {
	// Start with the base path
	var path string

	// These are all path components to check.
	// They are loaded in sequence and have a trailing &
	var Descending string
	var EndKey string
	var Group string
	var GroupLevel string
	var IncludeDocuments string
	var InclusiveEnd string
	var Key string
	var Limit string
	var Reduce string
	var Skip string
	var Stale string
	var StartKey string
	var UpdateSequence string

	if pq.IncludeDocuments {
		IncludeDocuments = "include_docs=true&"
		if pq.Conflicts {
			IncludeDocuments += "conflicts=true&"
		}

		if pq.Attachments {
			IncludeDocuments += "attachments=true&"
		}

		if pq.AttachmentEncodingInfo {
			IncludeDocuments += "att_encoding_info=true&"
		}
	}

	if pq.Descending {
		Descending = "descending=true&"
	}

	if pq.EndKey != "" {
		EndKey = fmt.Sprintf("endkey=\"%s\"&", pq.EndKey)
		if pq.EndKeyDocID != "" {
			EndKey += fmt.Sprintf("endkey_docid=\"%s\"&", pq.EndKeyDocID)
		}
	}

	if pq.StartKey != "" {
		StartKey = fmt.Sprintf("startkey=\"%s\"&", pq.StartKey)
		if pq.EndKeyDocID != "" {
			StartKey += fmt.Sprintf("startkey_docid=\"%s\"&", pq.StartKeyDocID)
		}
	}

	if !pq.Reduce {
		Reduce = fmt.Sprintf("reduce=false&")
	} else {
		// Group cannot exist when reduce is disabled.
		if pq.Group {
			Group = "group=true&"

			// Horrible but effective
			if len(pq.Keys) > 0 {
				keys := ""
				for _, v := range pq.Keys {
					keys += fmt.Sprintf("\"%s\",", v)
				}
				keys = keys[:len(keys)-1]
				Group += fmt.Sprintf("keys=[%s]&", keys)
			}
		}
	}

	if pq.GroupLevel > 0 {
		GroupLevel = fmt.Sprintf("group_level=%d&", pq.GroupLevel)
	}

	if !pq.InclusiveEnd {
		InclusiveEnd = "inclusive_end=false&"
	}

	if pq.Key != "" {
		Key = fmt.Sprintf("key=\"%s\"&", pq.Key)
	}

	if pq.Limit > 0 {
		Limit = fmt.Sprintf("limit=%d&", pq.Limit)
	}

	if pq.Skip > 0 {
		Skip = fmt.Sprintf("skip=%d&", pq.Skip)
	}

	if pq.Stale != "" {
		st := strings.ToLower(pq.Stale)
		if st == "update_after" || st == "ok" {
			Stale = fmt.Sprintf("stale=\"%s\"&", st)
		}
	}

	if pq.UpdateSequence {
		UpdateSequence = fmt.Sprintf("reduce=true&")
	}

	path = fmt.Sprintf(
		"?%s%s%s%s%s%s%s%s%s%s%s%s%s",
		Descending,
		EndKey,
		Group,
		GroupLevel,
		IncludeDocuments,
		InclusiveEnd,
		Key,
		Limit,
		Reduce,
		Skip,
		Stale,
		StartKey,
		UpdateSequence,
	)

	last := string(path[len(path)-1])

	// Aesthetic
	switch last {
	case "&":
		path = path[:len(path)-1]
		break
	case "?":
		path = path[:len(path)-1]
		break
	}

	return path
}
