// Copyright (c) 2015 Sentential Pty Ltd
// All rights reserved.
//
// Released under zlib/png licence (see LICENCE)

package let

// DesignDocumentOptions represents the Options stored within a design document.
type DesignDocumentOptions struct {
	// need more information/examples
}
