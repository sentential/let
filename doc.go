// Copyright (c) 2015 Sentential Pty Ltd
// All rights reserved.
//
// Released under zlib/png licence (see LICENCE)

/*
Package let is a CouchDB 2.0 (developer preview) interface. It uses native Go
HTTP(S) client code to interface with the builds as they update.

It is a continual work in progress and should be considered as such.  However, the implemented API (as of 2015-10-11 UTC) is considered stable within the  master branch of the repository.

The primary aims are to create a structured/tiered access pattern to CouchDB,
to enable easy, clean, logical, and idiomatic access to CouchDB without needing
to resort to direct HTTP(S) calls.

Let (Go) server instances also expose all HTTP calls CouchDB uses (in the format
of `HTTP<VERB>`).
*/package let
