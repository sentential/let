# Feature List

This document seeks to list what exists and what should exist.  It is divided into components based on the following hierarchy.

## Feature Hierarchy

This structure should be considered incomplete as there are many other features missing (predominantly from the Design Document).  Anything represented in verbose (i.e. `like this`) should be considered unimplemented but known.


* Server
	* Database
		* `Mango`
		* Design Document
			* `Filters`
			* `Options`
			* `Rewrite`
			* `Shows`
			* Views
		* Document
			* Attachments

* `It` - MapReduce Query Server

## Completed

* Server
	* Cookie Authentication

* Database
	* Document creation (`.Save([]byte)`, `.NewDocument()`)
	* Creation (via Server).
	* Read (basic information).
	* Database deletion (via Server).
	* `_all_docs` access (via pagination object).

* Document
	* Create (see `Database`, supra);
	* Read (`Get`, `GetLatest`);
	* Update (`Save`, `SaveRaw`);
	* Delete (`Delete`);
	* Copy (`Copy`, `CopyLatest`, `CopyRevision`);
	* Batch save/POST.

* Design Document
	* Basic decoding;
	* Access to views.

* Views
	* Complex Query (via structure);
	* Pagination: `Next()`/`Previous()`;
	* View Index information.

* Attachments
	* Can see encoding via Views.


## TODO

* Server
	* OAuth (not feasible as present)

* Database
	* Clean
	* Compact
	* Replication

* Document

* Design Document
	* Access to Rewrites

* Views
	* Addition/removal of distinct views;
	* Loading from disk.

* Mango

* Rewrites (Design Document component)

* Shows (Design Document component)

* Filters (Design Document component)

* Attachments
	* Uploading
	* Downloading
	* Access
	* Encoding Checking

## Wishlist

* Hot-reloading query server (would require ast and parser work to make it sane, safe, or even possible);
* `_changes` related server/watcher code (`let/it`).