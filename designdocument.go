// Copyright (c) 2015 Sentential Pty Ltd
// All rights reserved.
//
// Released under zlib/png licence (see LICENCE)

package let

import (
	"encoding/json"
	"errors"
	"fmt"
)

// DesignDocumentInfo represents values returned by `/db/_design/design-doc/_info`
type DesignDocumentInfo struct {
	Name      string                      `json:"name"`       // Name
	ViewIndex DesignDocumentViewIndexInfo `json:"view_index"` // View Index
}

// DesignDocumentViewIndexInfo represents values returned by the view_index field of `/db/_design/design-doc/_info`
type DesignDocumentViewIndexInfo struct {
	CompactRunning bool   `json:"compact_running"`
	DataSize       int    `json:"data_size"`
	DiskSize       int    `json:"disk_size"`
	Language       string `json:"language"`
	PurgeSequence  string `json:"purge_seq"`
	Signature      string `json:"signature"`
	UpdateSequence int    `json:"update_seq"`
	UpdaterRunning bool   `json:"updater_running"`
	WaitingClients int    `json:"waiting_clients"`
	WaitingCommit  bool   `json:"waiting_commit"`
}

// DesignDocument_View is defined in designdocument_view.go

// DesignDocument represents the data found in a design document.
type DesignDocument struct {
	db                 *Database                      `json:"-"`                   // Database (user has no reason to access this)
	Name               string                         `json:"-"`                   // Design document name
	Path               string                         `json:"-"`                   // Design document path
	rawBytes           []byte                         `json:"-"`                   // Raw JSON source
	Language           string                         `json:"language"`            // Language (defaults to JavaScript if missing)
	Options            DesignDocumentOptions          `json:"options"`             // Default options (not yet implemented)
	Filters            map[string]string              `json:"filters"`             // Key is the function name; value is a function's source (stringified)
	Lists              map[string]string              `json:"lists"`               // Key is the function name; value is a function's source (stringified)
	Rewrites           []*DesignDocumentRewrite       `json:"rewrites"`            // From/To (as per the docs)
	Shows              map[string]string              `json:"shows"`               // Key is the function name; value is a function's source (stringified)
	Updates            map[string]string              `json:"updates"`             // "Update functions definition"
	Views              map[string]*DesignDocumentView `json:"views"`               // Views
	ValidateDocUpdates string                         `json:"validate_doc_update"` // Validate document update function source; value is a function's source (stringified)
}

func (dd *DesignDocument) updateRawBytes() (err error) {
	dd.rawBytes, err = json.Marshal(dd)
	return err
}

// Save stores a design document (using raw data).
func (dd *DesignDocument) Save() (err error) {
	err = dd.updateRawBytes()
	if err != nil {
		return
	}
	_, err = dd.db.Save(dd.rawBytes)
	return
}

// Reload triggers a reload.
func (dd *DesignDocument) Reload() (err error) {

	status, _, body, err := dd.db.server.HTTPGet(dd.Path)

	if status != 200 {
		err = errors.New("document missing")
	}

	if err != nil {
		return err
	}

	err = json.Unmarshal([]byte(body), dd)
	if err != nil {
		return err
	}

	// Notify all views that they have a parent and a name.
	for k, v := range dd.Views {
		v.Name = k
		v.dd = dd
	}

	return nil
}

// GetView obtains a design document.
func (dd *DesignDocument) GetView(name string) (*DesignDocumentView, error) {
	if v, ok := dd.Views[name]; ok {
		return v, nil
	}
	return nil, errors.New("no such view")
}

// Info returns `/db/_design/design-doc/_info`
func (dd *DesignDocument) Info() (*DesignDocumentInfo, error) {
	info := &DesignDocumentInfo{}
	status, _, body, err := dd.db.server.HTTPGet(
		fmt.Sprintf(
			"%s/_info",
			dd.Path,
		),
	)

	if status != 200 {
		return nil, errors.New("unable to obtain design document information")
	}

	if err != nil {
		return nil, err
	}

	err = json.Unmarshal([]byte(body), info)
	if err != nil {
		return nil, err
	}

	return info, err
}

// String performs stringification for sprintf (etc.)
func (dd *DesignDocument) String() string {
	err := dd.updateRawBytes()
	if err != nil {
		return "[error stringifying design document]"
	}
	return string(dd.rawBytes)
}
