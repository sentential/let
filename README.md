# Let (Go) - Go <-> CouchDB

[![GoDoc](https://godoc.org/bitbucket.org/sentential/let?status.svg)](https://godoc.org/bitbucket.org/sentential/let)

This is a work in progress.  Consider it incomplete.

*Current tests indicate the code is functional but bear in mind a major refactor and feature addition has just been committed.  Consider the master branch to be unstable until further notice.  This code is shared for general use/access rather than as a suggestion of stability.*

It use currently used internally for prototype development and will expand as CouchDB 2.0 moves from developer preview into something more.  It is being released to the public under the zlib/png licence as part of its development.

Things worth noting:

* Targets CouchDB 2.0 (developer preview) and NOT the existing builds (1.6);
* Has SSL/TLS support CouchDB SSL/TLS (albeit entirely untested);
* Cookie Authentication (optional but tested);
* Public mode (no auth/admin party);
* Only Go built-ins are used.

## The general approach

The approach adopted in this library is as follows:

* The preference is to store, update, and iterate over pointers to objects (to avoid copying);
* Everything should look a bit like this: `<object>, err :=`;
	* Errors are a good thing, checking if they are non-nil is all you should really need;
	* Errors are to be "cleaned up" if they match a known oddity (e.g. 404 status pages are translated into 403 "couchy" errors for a lack of access);
* I speak British English (well, Australian English), 
* Access should be structured like the database (and layered like an Ogre).  Thus it follows that:
	* Server objects contains databases;
	* Databases objects contain documents and design documents;
	* Design documents contain views (and other structures);
		* Design documents are first class citizens and no should use their own queries (back to the database);
	* Design document views have complex query syntax;
			* Views shouldn't reuse document access semantics, and should use their own direct GET calls (etc.);
		* Paginated views have the ability to step forward (`Next()`) or backward (`Previous()`), and need to be (re)loaded (`Load()`);
		* Query objects are needed to keep views sane (`View_Page_Query`).
	* Documents (of all kinds) contain data but only resolve to ID (`_id`) and Revision (`_rev`), keeping the internal state as a string in the document object's fields (`RawDocument`);
		* Document semantics, structure, and manipulation should be done on a per-application level; this isn't Python, interface{} hacks are seriously ugly.

## API

The API is let is designed to be as simple as possible.

### The basics

To get a server:

	// For non-TLS
	server := let.Connect(couch_url)

	// For TLS (this should be your default connection)
	// Note: if the latter values are blank it will attempt non-TLS.
	server := let.ConnectTLS(couch_url,
	  path_to_cert_file,
	  path_to_key_file,
	  path_to_ca_file,
	)

To authenticate (optional where your database or server doesn't need it):

	// Authentication using Cookies
	// Non-TLS fires this over a wire as expected, so keep that in mind.
	server.SetCookieAuth(username, password)

	// OAuth is presently unavailable as it seems to want you to sign in before
	// you can sign in ... (I know, I thought the same thing.)

To get a database:

	// Gets a database only if it exists
	db, err := server.Get(name)

	// Gets a database or creates it if it's missing
	db, err := server.GetOrCreate(name)

	// Creates a database (only if it doesn't exist)
	db, err := server.Create(name)


To get a document:

	// Gets a document from a given database
	doc, err := db.Get(id)

To save:

	// Saves []byte as a document
	// _id and _rev are extracted using encoding/json and interface{}
	// If no _rev is present or it's blank, _rev will be removed;
	// If no _id is present, or it's blanked, _id will be given a UUID.
	db.Save(json_as_byte_array)

## Licence

The licence for this library is: `zlib/png` (see LICENCE).

This licence was selected for what it brings and what it does not restrict; it is a simple, clean, neat licence which allows the use of the library liberally.

## Bugs

If you find one please file it.  Better yet, if you know how to fix it, and have the time, please send a pull request with a fix.

## Changes

* 2015-10-12 - Pagination update
	* `Summary`
		* Minor changes to pagination to allow for the same code to be re-used (to a degree) elsewhere (e.g. `_all_docs`);
		* Renaming and interfacing of things (to prevent too many changes and to keep things consistent).
	* `Added`
		* `Pagination` related code;
			* `Pagination` interface (allowing the View pagination to remain largely unchanged);
			* `PaginationJSON` for common variables in non-reduced views/pagination instances.
		* `AllDocuments` and `AllDocumentsQuery` (to `Database`);
			* `AllDocumentsPagination` (using the Pagination interface);
	* `Changed`
		* `NewViewQuery()` -> `NewQuery()`;
		* `ViewPaginationQuery` -> `PaginationQuery`;
		* `ViewPagination` now follows the interface.

* 2015-10-11 - Minor update
	* `Added`
		* Bulk document object (`BulkDocuments`);
		* `GetUUIDs(count)` to the server interface.
		* Added directory for `tests/suite` (but still planning implementation).
	* `Changed`
		* Moved tests into subdirectories.

* 2015-10-11 - Lint Update #2 (run to a confidence of 0.25)
	* `Changed`
		* Collapsed `Copy` into a direct call to `CopyRevision`;
		* Tweaked internals of `Next` and `Previous` for views;
		* Comment strings (in violation of the linter).

* 2015-10-11 - Lint Update (prompted by using `golint` on the merged code)
	* `Added`
		* `designdocumentrewrite.go`
	* `Changed` - Breaking API changes (in compliance with linting)
		* Stylistic changes;
		* Added `fmt.Errorf` where relevant (retiring Sprintf + errors.New);
		* Functional changes (dropping `else` logic where irrelevant);
		* Collapsed all server related code into `server.go`;
		* Collapsed `CopyLatest` into a direct call to `CopyRevision`;
		* Split `rewrite` functionality into its own document (for future use);
		* Split `options` functionality into its own document (for future use);
		* Renamed all files to remove underscores (it does make them harder to read);
		* Renamed all internal HTTP functions (what I was trying to avoid with the underscore):
			* `HTTP_GET` -> `HTTPGet`;
			* `HTTP_PUT` -> `HTTPPut`;
			* `HTTP_POST` -> `HTTPPost`;
			* `HTTP_DELETE` -> `HTTPDelete`;
			* `HTTP_COPY` -> `HTTPCopy`;
			* `HTTP_HEAD` -> `HTTPHead`.
		* Renamed all underscore violations (31 violations);
			* `Info_Database_Sizes` -> `DatabaseInfoSizes`;
			* `Info_Database_Other` -> `DatabaseInfoOther`;
			* `Info_Database` -> `DatabaseInfo`;
			* `DesignDocument_Options` -> `DesignDocumentOptions`;
			* `DesignDocumentRewrite` -> `DesignDocumentRewrite`;
			* `DesignDocumentInfo` -> `DesignDocumentInfo`;
			* `Info_DesignDocument_ViewIndex` -> `DesignDocumentViewIndexInfo`;
			* `DesignDocument_View` -> `DesignDocumentView`;
			* `DesignDocument_View_Row` -> `DesignDocumentViewRow`;
			* `ServerInfo_Vendor` -> `ServerInfoVendor`;
			* `Session_UserCtx` -> `SessionUserContext`;
			* `Session_Info` -> `SessionInfo`;
			* `View_Page` -> `ViewPagination`;
			* `View_Page_Query` -> `ViewPaginationQuery`;
			* Various params (e.g. `doc_id` -> `docID`).

* 2015-10-11
	* `Added`
		* Server `IsAuthenticated()` function (returns boolean);
		* FEATURES.md to better track things at a macro scale;
		* Document `Copy()` functionality  (with `CopyRevision` and `CopyLatest` shortcuts/helpers);
		* Document `Delete()` functionality (with `ForceDelete` shortcut/helper).
	* `Removed`
		* Removed authentication timeout/removal (401/403/404) semantic catcher - users will now have to detect their own failed authentication states if in doubt.
* 2015-10-10
	* `Added`
		* Public repository;
		* Stable API (I hope);
		* Weird semantics catch-all -- 404 arises where 403 should -- the document/database/url returns as `missing` when it is a redirect.  Instead of moving the status and throwing the error let catches this and returns a 403 so you know why.  This has been tweaked a bit to stop it from landing on pages it shouldn't;
		* Query object for Views (`View_Page_Query`, accessible via `NewViewQuery()`);
		* Two quick/simple tests (`let-doc-check.go`, `let-view-check.go`).
	* `Changed`
		* Existing functions pushed into a sane API;
		* *Massive* refactor (moved things for a 'class-like' structuring) - all functions of objects are in their respective files; structs are in their respective files (etc.);
		* Fixed some comment strings and added status specificity to View_Page;
	* `Removed`
		* Server to document interfaces;
		* Database to view interfaces (now accessible via `_design` documents);
		* Database to pagination (now accessible via `_view` instances).
		* Ugly, strange, and hacky `_design` and `_view` semantics;
		* Various outdated tests;
		* That bamboozling comment over the old server object (now `ConnectTLS`) with the infinitive split by a gerund masquerading as an adverb (probably caused by hyperediting and the early fluid nature of the function);
		* Various (redundant) checks (e.g. `_rev` is no longer required in `save` (CouchDB error returns instead if the system is asked to find the error)).


## TODO

* Attachment support (I haven't needed it yet);
* Some sort of test-suite;
  * Moved to automation.
* Test SSL (another couple of VMs should do it);
* Test behaviour with latency;
* Clean up some internals, check for bottlenecks, {ref,pointer} <-> value mistakes are probably visible;
* Document anything lacking/missing documentation (including the obvious things); also add 'doc.go' for clarity/header.

### Structure/refactoring

Prior to release a few major changes were introduced.  These introduced some inconsistencies beyond the 'hacky' nature of the code.  A lot of the bad this brought about is here:

* Make error handling a little more standard (fix let internal/deployed/release divides);
* Do status checks;
* Move to `HTTP_<VERB>` requests where appropriate.

### Code cleanliness (and cleaning)

This began as internal hacky code for a given purpose; it was quite literally hacked together around basic HTTP call wrappers.  TLS was added for HAProxy client authentication support, stripped, and then restored for HTTPS enabled CouchDB (and left untested).  A lot of other changes ranging from minor shifts in design pattern to a rework of legacy functions into a structured approach mean there is probably a lot to be fixed.  Here is how this is maintained:

* Values are de/serialised into structures;
* By default all structures are returned by pointer (avoiding replication when writing code using `:=` assignment syntax);
* Comments are removed as the component becomes stable enough, though some comments are retained for transparency.


### "Working"

This list should appear to be in some sort of working order once the development branch gets updated/pushed/created.

* Complex query semantics for pages with complex query syntax (most probably with the syntax of `<Obj>_Query`, following `View_Page_Query`);
* Remainder of design document entries (helpers, etc.).


#### The Dream (aka the Wishlist)

The wishlist is derived from comments, fragments of code, and other things from within the repository.

* Change watching utility/service (proposed: dir/name `let/it`):
	* Filters (to clear up the swirling storm inside);
	* Appropriate security: conceal/don't feel (appropriate 'rejection' without admitting to things existing/don't use forwards; throw a 404);
	* Optional rule system (no rules for me/kingdom of isolation).
* Map/Reduce utility/service (name TBA):
	* Explore potential marshy areas around this idea.


It is worth noting that any `let/it` references found in the directory will be a dangling until internal and external code issues are fixed.


### "Not on the list"

Things not being taken too seriously are ...

* Any documented CouchDB API `HEAD` call (Sentential hasn't once used one of these calls; are these even in other libraries?);
* OAuth (at least until it's fixed and doesn't require authentication to access the end-point ...).

### Unplanned, unreleased, and disabled

This list is comprised predominantly of things that aren't actively testable or have shown themselves to be unstable in the last few months.  The exceptions to this are the items listed on the watch list (below).

* Various interfaces not currently used (because I won't know if they break!):
	* Complex view/design functions;
	* Helpers for populating views without constructing them manually.

### Watch list

The following are points which are under observation and consideration.  However, just because they are on the list does not mean they are potential candidates for inclusion; it merely indicates they are of some interest.  (Candidates for inclusion will appear at some stage listed somewhere in this README).

* OAuth currently requires authentication to be accessed (404 returns with a redirect);
* Replication and other functions (some GET calls exist, but POST, PUT, etc., do not).  Consider these disabled or partially implemented (and unlikely to be implemented in the near future).  These will make the list when they are more testable within the environment (i.e. have a reason to be tested);
* Design documents (additional features, shows, etc.) -- helpers.