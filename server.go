// Copyright (c) 2015 Sentential Pty Ltd
// All rights reserved.
//
// Released under zlib/png licence (see LICENCE)

package let

import (
	"bytes"
	"crypto/tls"
	"crypto/x509"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/cookiejar"
)

// Server connection (non-SSL)
type Server struct {
	// In the format of http(s)://<host>:<port>(/<prefix>)
	// Should not contain a trailing slash;
	// prefix only where required (multiple DBs on the virtual hosts, etc.)
	ServerURL string

	// HTTP Client
	ClientHTTP *http.Client

	// Username
	username string

	// Password
	password string

	// Bad authentication
	badAuth bool
}

// ----------------------------------------------------------------------------
// Information
// ----------------------------------------------------------------------------

// ServerInfoVendor provides basic information about the vendor of the build.
type ServerInfoVendor struct {
	// Typically "The Apache Software Foundation"
	Name string
}

// ServerInfo provides basic server information.
type ServerInfo struct {
	// Messaage
	CouchDB string

	// UUID
	Version string

	// Vendor information
	Vendor ServerInfoVendor
}

// ServerActiveTask is a representation of the json data
// Currently unknown if this works.
type ServerActiveTask struct {

	//Processed changes
	ChangesDone uint64 `json:"changes_done"`

	// Source database
	Database string

	// Process ID
	PID string

	// Current percentage progress (0-100)
	Progress uint8

	// Task start time as unix timestamp
	StartedOn uint64 `json:"started_on"`

	// Task status message
	Status string

	// Task name
	Task string

	// Total changes to process
	TotalChanges uint64 `json:"total_changes"`

	// Operation Type
	Type string

	// Unix timestamp of last operation update
	UpdatedOn uint64 `json:"updated_on"`
}

// DatabaseUpdate is a Go struct representing _db_updates
type DatabaseUpdate struct {
	// Database name
	DatabaseName string `json:"db_name"`

	// Event operation status
	OK bool

	// A database event; one of created, updated, deleted
	Type string
}

// Replicate performs replication between a source and target.
// This function is untested and has been created based on the documentation.
func (server *Server) Replicate(source string, target string) (err error) {
	// serverReplicate is an internal replication object.
	type serverReplicate struct {
		source string
		target string
	}
	repl := serverReplicate{source, target}
	_bytes, _err := json.Marshal(repl)

	if _err != nil {
		err = _err
		return
	}

	inBody := bytes.NewReader(_bytes)

	_, _, _, err = server.HTTPPost("/_replicate", inBody)

	return
}

// GetMembership returns membership information from the _membership API call.
// It appears to function as intended.
func (server *Server) GetMembership() (memberships map[string][]string, err error) {
	status, _, body, err := server.HTTPGet("/_membership")

	if err != nil {
		return
	}

	if status != 200 {
		err = parseErrorCouchDB(status, &body)
		return
	}

	err = json.Unmarshal([]byte(body), &memberships)
	if err != nil {
		log.Println("Unable to decode '/_membership'.")
		log.Println(body)
	}
	return
}

// GetAllDatabaseUpdates lists all updates from _db_updates using a list of DatabaseUpdate objects.
func (server *Server) GetAllDatabaseUpdates() (updates []DatabaseUpdate, err error) {
	status, _, body, err := server.HTTPGet("/_db_updates")

	if err != nil {
		return
	}

	if status != 200 {
		err = parseErrorCouchDB(status, &body)
		return
	}

	err = json.Unmarshal([]byte(body), &updates)
	if err != nil {
		log.Println("Unable to decode '/_db_updates'.")
		log.Println(body)
	}
	return
}

// GetAllDatabases returns a list of database names based on _all_dbs; the results may vary based on access control.
func (server *Server) GetAllDatabases() (names []string, err error) {
	status, _, body, err := server.HTTPGet("/_all_dbs")

	if err != nil {
		return
	}

	if status != 200 {
		err = parseErrorCouchDB(status, &body)
		return
	}

	err = json.Unmarshal([]byte(body), &names)
	if err != nil {
		log.Println("Unable to decode '/_all_dbs'.")
		log.Println(body)
	}
	return
}

// GetActiveTasks returns a list of active server tasks.
// This is currently untested and has been drafted based on documentation.
func (server *Server) GetActiveTasks() (tasks []ServerActiveTask, err error) {
	status, _, body, err := server.HTTPGet("/_active_tasks")

	if err != nil {
		return
	}

	if status != 200 {
		err = parseErrorCouchDB(status, &body)
		return
	}

	err = json.Unmarshal([]byte(body), &tasks)
	if err != nil {
		log.Println("Unable to decode '/_active_tasks'.")
		log.Println(body)
	}
	return
}

// GetUUID gets a single UUID from the server generator.
// This function is used by internal functions to avoid using POST for document creation.
func (server *Server) GetUUID() (string, error) {
	uuids, err := server.GetUUIDs(1)
	if len(uuids) == 1 && err == nil {
		return uuids[0], nil
	}
	return "", err
}

// GetUUIDs returns a number of UUIDs from the server.
func (server *Server) GetUUIDs(count int) ([]string, error) {
	_, _, body, err := server.HTTPGet(fmt.Sprintf("/_uuids?count=%d", count))

	if err != nil {
		return nil, err
	}

	// UUIDS; very simple.
	type targetJSON struct {
		Array []string `json:"uuids"`
	}

	uuids := targetJSON{}
	err = json.Unmarshal([]byte(body), &uuids)
	if err != nil {
		return nil, err
	}
	return uuids.Array, nil
}

// Info gets basic server information
func (server *Server) Info() (info *ServerInfo, err error) {
	status, _, body, err := server.HTTPGet("/")

	if err != nil {
		// This is a HUGE problem.
		log.Println("Unable to access server information.")

		// Kill the info.
		info = nil

		// Don't kill the instance; maybe it's just a bad server in a collection;
		// give the user/dev a chance to notice the dead server.
		return
	}

	if status != 200 {
		info = nil
		err = parseErrorCouchDB(status, &body)
		return
	}

	info = &ServerInfo{}

	err = json.Unmarshal([]byte(body), info)
	if err != nil {
		log.Println("Unable to decode '/'.")
		// This could be on us, so don't fail out just because it errors.
	}
	return
}

// ----------------------------------------------------------------------------
// Connection
// ----------------------------------------------------------------------------

// Connect creates a server connection without TLS
func Connect(uri string) (server *Server, err error) {
	server = &Server{}
	server.ServerURL = uri

	// Setup the cookie jack
	cookieJar, _ := cookiejar.New(nil)

	// Create the server
	server.ClientHTTP = &http.Client{
		Jar: cookieJar,
	}

	// Get info (a quick/cheap/dirty test)
	info, err := server.Info()

	// If the server information comes back bad, it's invalid.
	// Handle this accordingly.
	if info == nil || err != nil {
		server = nil
	}

	return
}

// ConnectTLS creates a server connection with TLS is available;
// if the TLS fields are blank (i.e. "") a non-TLS connection is used.
//
// This connection call should be used when the application is unsure
// and is using a configuration file to load the server.
func ConnectTLS(uri string, certFile string,
	keyFile string, caFile string) (server *Server, err error) {

	// Create a server instance.
	server = &Server{}

	// Stores the URI
	server.ServerURL = uri

	cookieJar, _ := cookiejar.New(nil)

	// We check for the cert first - to avoid nesting this will fatal out.
	// This is also a good idea for security, though it's likely it will fail
	// if the HAProxy configuration on the other end is remotely sane.
	if certFile != "" || keyFile != "" {
		// Check everything is valid
		if certFile == "" || keyFile == "" {
			log.Println("Bad SSL configuration: incomplete input.")
			server = nil
			return
		}

		cert, err := tls.LoadX509KeyPair(certFile, keyFile)
		if err != nil {
			log.Println("Bad SSL configuration: failed on 'LoadX509KeyPair'.")
			server = nil
			return server, err
		}

		caCertPool := x509.NewCertPool()

		// Only use this if it's provided, otherwise assume the user knows
		// what they're doing (i.e. it's in the root/common/system config.)
		if caFile != "" {
			// Load the caCert - no harm if it fails, as we'll fail below instead.
			caCert, err := ioutil.ReadFile(caFile)
			if err != nil {
				log.Println(err)
			} else {
				caCertPool.AppendCertsFromPEM(caCert)
			}
		}

		// Simple enough, and it should work happily.
		tlsConfig := &tls.Config{
			Certificates: []tls.Certificate{cert},
			RootCAs:      caCertPool,
		}
		tlsConfig.BuildNameToCertificate()

		// Build the transport
		transport := &http.Transport{
			TLSClientConfig: tlsConfig,
		}

		// Build the TLS client for SSL.
		server.ClientHTTP = &http.Client{
			Transport: transport,
			Jar:       cookieJar,
		}
	} else {
		server.ClientHTTP = &http.Client{
			Jar: cookieJar,
		}
	}

	// Get info (a quick/cheap/dirty test)
	info, err := server.Info()

	// If the server information comes back bad,
	// it's not a valid server (or this library is out of date)
	if info == nil || err != nil {
		server = nil
	}

	// Returns the server
	return
}

// ----------------------------------------------------------------------------
// Database manipulation
// ----------------------------------------------------------------------------

// Create attempts to create a database of a given name.
func (server *Server) Create(name string) (db *Database, err error) {
	db = nil
	if server.Exists(name) {
		return nil, errors.New("database already exists")
	}

	status, _, body, err := server.HTTPPut("/"+name, nil)
	if err == nil {
		if couchIsOk(status, &body) {
			db, err = server.Get(name)
			if err != nil {
				db = nil
			}
		} else {
			// Not sure this is right, but it should be *okay* if we make it
			// this far.  Needs more testing.
			err = parseErrorCouchDB(status, &body)
		}
	}

	return
}

// Exists checks if the database exists.
func (server *Server) Exists(name string) (exists bool) {
	exists = false
	db, err := server.Get(name)
	if err == nil {
		if db != nil {
			exists = true
		}
	}
	return
}

// GetOrCreate gets a database of the given name; it is created if it is not found and access allows.
func (server *Server) GetOrCreate(name string) (db *Database, err error) {
	db, err = server.Get(name)
	if db == nil {
		err = nil
		db, err = server.Create(name)
	}

	// Catch any errors and remove the DB from scope if it's not nil
	if err != nil && db != nil {
		db = nil
	}
	return
}

// Get returns an existing database (it does not attempt to create if the requested database is not found).
func (server *Server) Get(name string) (db *Database, err error) {
	// Create it
	db = &Database{server, name, "/" + name, DatabaseInfo{}}

	err = db.UpdateInfo()

	if err != nil {
		db = nil
	}
	return
}

// Delete removes a database
func (server *Server) Delete(db *Database) (err error) {
	if db != nil {
		name := db.Name
		status, _, body, er := server.HTTPDelete(name)

		// We only care if this comes back true
		if server.Exists(name) {
			err = fmt.Errorf("failed to remove database.\nstatus: %d\nbody: %s\ninternal/http error: %s\n", status, body, er)
		}
	}
	return err
}

// ----------------------------------------------------------------------------
// HTTP Verbs
// ----------------------------------------------------------------------------

// HTTPGet performs the 'GET' requests returning status code, headers, body, and error (if any).
// path must include the query (if required)
func (server *Server) HTTPGet(path string) (status int, header *http.Header, body string, err error) {

	// Make the URI
	uri := server.ServerURL + "/" + path

	// Run GoLang's Get
	resp, err := server.ClientHTTP.Get(uri)

	// Fail out
	if err != nil {
		status = 0
		return
	}

	// No failure means this eventually needs to be closed
	defer resp.Body.Close()

	// Get the header
	header = &resp.Header

	status = resp.StatusCode

	// Get the body
	tmpBody, err := ioutil.ReadAll(resp.Body)
	body = string(tmpBody)

	return
}

// HTTPHead performs the 'HEAD' requests returning status code, headers, and error (if any).
func (server *Server) HTTPHead(path string) (status int, header *http.Header, err error) {
	uri := server.ServerURL + "/" + path

	// Run GoLang's Head
	resp, err := server.ClientHTTP.Head(uri)

	// Fail out
	if err != nil {
		status = 0
		return
	}

	// Get the header
	header = &resp.Header

	status = resp.StatusCode

	return
}

// HTTPPost performs the 'POST' requests returning status code, headers, body, and error (if any).
func (server *Server) HTTPPost(path string, inBody io.Reader) (status int, header *http.Header, body string, err error) {
	uri := server.ServerURL + "/" + path

	// Run GoLang's Post
	resp, err := server.ClientHTTP.Post(uri, "application/json", inBody)

	// Fail out
	if err != nil {
		status = 0
		return
	}

	// No failure means this eventually needs to be closed
	defer resp.Body.Close()

	// Get the header
	header = &resp.Header

	// Get the code
	status = resp.StatusCode

	// Get the body
	tmpBody, err := ioutil.ReadAll(resp.Body)
	body = string(tmpBody)

	return
}

// HTTPPut performs the 'PUT' requests returning status code, headers, body, and error (if any).
func (server *Server) HTTPPut(path string, inBody io.Reader) (status int, header *http.Header, body string, err error) {
	uri := server.ServerURL + "/" + path

	// Craft a PUT request with the body
	req, err := http.NewRequest("PUT", uri, inBody)
	if err != nil {
		status = 0
		return // users can use the err
	}

	// We want JSON back
	req.Header.Set("Content-Type", "application/json")

	// Do request
	resp, err := server.ClientHTTP.Do(req)

	// Fail out
	if err != nil {
		status = 0
		return // users can use the err
	}

	// No failure means this eventually needs to be closed
	defer resp.Body.Close()

	// Get the header
	header = &resp.Header

	// Get the code
	status = resp.StatusCode

	// Get the body
	tmpBody, err := ioutil.ReadAll(resp.Body)
	body = string(tmpBody)

	return
}

// HTTPDelete performs the 'DELETE' requests returning status code, headers, body, and error (if any).
func (server *Server) HTTPDelete(path string) (status int, header *http.Header, body string, err error) {
	uri := server.ServerURL + "/" + path

	// To stop the request from complaining
	var tmp []byte
	tmpReader := bytes.NewReader(tmp)

	// Craft a PUT request with the body
	req, err := http.NewRequest("DELETE", uri, tmpReader)
	if err != nil {
		status = 0
		return // users can use the err
	}

	// Give it a header
	req.Header.Set("Content-Type", "application/json")

	// Do request
	resp, err := server.ClientHTTP.Do(req)

	// Fail out
	if err != nil {
		status = 0
		return // users can use the err
	}

	// No failure means this eventually needs to be closed
	defer resp.Body.Close()

	// Get the header
	header = &resp.Header

	// Get the code
	status = resp.StatusCode

	// Get the body
	tmpBody, err := ioutil.ReadAll(resp.Body)
	body = string(tmpBody)

	return
}

// HTTPCopy performs the 'COPY' requests returning status code, headers, body, and error (if any).
func (server *Server) HTTPCopy(path string, destinationDocumentID string) (status int, header *http.Header, body string, err error) {
	uri := server.ServerURL + "/" + path

	// To stop the request from complaining
	var tmp []byte
	tmpReader := bytes.NewReader(tmp)

	// Craft a PUT request with the body
	req, err := http.NewRequest("COPY", uri, tmpReader)
	if err != nil {
		status = 0
		return // users can use the err
	}

	// We want JSON back
	req.Header.Set("Content-Type", "application/json")

	// Give it a header for destination
	req.Header.Set("Destination", destinationDocumentID)

	// Do request
	resp, err := server.ClientHTTP.Do(req)

	// Fail out
	if err != nil {
		status = 0
		return // users can use the err
	}

	// No failure means this eventually needs to be closed
	defer resp.Body.Close()

	// Get the header
	header = &resp.Header

	// Get the code
	status = resp.StatusCode

	// Get the body
	tmpBody, err := ioutil.ReadAll(resp.Body)
	body = string(tmpBody)

	return
}
