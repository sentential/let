// Copyright (c) 2015 Sentential Pty Ltd
// All rights reserved.
//
// Released under zlib/png licence (see LICENCE)

package let

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"strconv"
	"strings"
)

// Document is a json de/serialisation object of a CouchDB document.
// It only contains _id and _rev; the RawDocument should be used by your application.
type Document struct {
	database    *Database `json:"-"`              // unserialised pointer to the parent database (internal only)
	ID          string    `json:"_id"`            // ID (_id) field
	Revision    string    `json:"_rev,omitempty"` // Revision (_rev) field
	RawDocument string    `json:"-"`              // Raw document as string (for printing or json-related use)
}

// documentRevision is an internal copy of a revision
type documentRevision struct {
	Rev    string
	Status string
}

// documentRevisionInformation is an internal copy of the revision information field
// (Used by GetRevisions and LatestRevision)
type documentRevisionInformation struct {
	ID                  string              `json:"_id"`
	Revision            string              `json:"_rev"`
	RevisionInformation []*documentRevision `json:"_revs_info"`
}

// Load retrieves the RawDocument data for processing (based on ID and Revision fields)
func (doc *Document) Load() error {
	path := fmt.Sprintf("%s/%s", doc.database.dbRoot, doc.ID)
	if doc.Revision != "" {
		path = fmt.Sprintf("%s?rev=%s", path, doc.Revision)
	}

	status, _, body, err := doc.database.server.HTTPGet(path)
	if status != 200 {
		err = errors.New("document missing")
	}

	if err == nil {
		doc.RawDocument = body
		json.Unmarshal([]byte(body), doc)
	}
	return err
}

// GetRevisions performs a revs_info lookup to find all available revisions.
func (doc *Document) GetRevisions() (revs map[string]string, err error) {
	path := fmt.Sprintf("%s/%s?revs_info=true", doc.database.dbRoot, doc.ID)
	status, _, body, err := doc.database.server.HTTPGet(path)
	if err == nil && status == 200 {
		var dri documentRevisionInformation
		err = json.Unmarshal([]byte(body), &dri)
		if err == nil {
			revs = make(map[string]string)
			for _, v := range dri.RevisionInformation {
				revs[v.Rev] = v.Status
			}
		}
	}
	return
}

// LatestRevision fetches the highest revision value.
// In the event of a collision the first found will be returned.
func (doc *Document) LatestRevision() string {
	rev := ""
	ver := 0
	path := fmt.Sprintf("%s/%s?revs_info=true", doc.database.dbRoot, doc.ID)
	status, _, body, err := doc.database.server.HTTPGet(path)
	if err == nil && status == 200 {
		var dri documentRevisionInformation
		err = json.Unmarshal([]byte(body), &dri)
		if err == nil {
			for _, v := range dri.RevisionInformation {
				rv, _ := strconv.Atoi(strings.Split(v.Rev, "-")[0])
				if rv > ver {
					rev = v.Rev
					ver = rv
				}
			}
		}
	}
	return rev
}

// CopyRevision performs a copy from the current document based on the given revision.
// The returned document is the NEW document and not the current document.
func (doc *Document) CopyRevision(revision string, targetID string) (*Document, error) {
	if doc.ID == "" {
		return nil, errors.New("this document has no _id, it cannot be copied; save then try again")
	}

	// Old rev
	oldRev := doc.Revision

	// Set latest if it's missing
	if revision == "" {
		doc.Revision = doc.LatestRevision()
	} else {
		doc.Revision = revision
	}

	path := fmt.Sprintf("%s/%s?rev=%s", doc.database.dbRoot, doc.ID, doc.Revision)

	if targetID == "" {
		uuid, err := doc.database.server.GetUUID()
		if err != nil {
			return nil, fmt.Errorf("target_id not specified and attempts to get a uuid have failed: %s", err)
		}
		targetID = uuid
	}

	fmt.Println(path)
	fmt.Println(targetID)

	status, _, body, err := doc.database.server.HTTPCopy(path, targetID)
	if err != nil {
		return nil, err
	}

	fmt.Println(status, body)

	if !couchIsOk(status, &body) {
		return nil, err
	}

	nd, err := doc.database.Get(targetID, "")

	// Restore
	doc.Revision = oldRev

	return nd, err
}

// CopyLatest performs a copy from the current document based on the latest revision.
// The returned document pointer is to the NEW document
func (doc *Document) CopyLatest(targetID string) (*Document, error) {
	return doc.CopyRevision(doc.LatestRevision(), targetID)
}

// Copy performs a copy based on the stored revision.
// The returned document pointer is to the NEW document
func (doc *Document) Copy(targetID string) (*Document, error) {
	return doc.CopyRevision(doc.Revision, targetID)
}

// ForceDelete attempts to remove the document regardless of revision.
func (doc *Document) ForceDelete() bool {
	doc.Revision = doc.LatestRevision()
	return doc.Delete()
}

// Delete attempts to remove the document based on the internal revision data.
// Errors are irrelevent here; all the user needs is 'did it happen?'
func (doc *Document) Delete() bool {

	// This would otherwise wipe the database...
	if doc.ID == "" {
		return false
	}

	if doc.Revision == "" {
		return false // this will fail and we don't want to force it.
	}

	path := fmt.Sprintf("%s/%s?rev=%s", doc.database.dbRoot, doc.ID, doc.Revision)

	status, _, _, err := doc.database.server.HTTPDelete(path)

	if err != nil {
		log.Printf("Encountered error `%s` while deleting document %s\n", err, doc.ID)
		return false
	}

	if !(status == 200 || status == 202) {
		log.Printf("Encountered undesired status code `%d` while deleting document %s\n", status, doc.ID)
		return false
	}

	return true
}

// SaveRaw saves the document based on current content (RawDocument).
// If successful it will do an internal update; on failure it returns false.
// This is a semi-legacy interface; it has been kept for weird use-cases.
//
// As with delete a fault is logged if present.
// Errors are irrelevent here; all the user needs is 'did it happen?'
func (doc *Document) SaveRaw() bool {
	e := doc.database.save(doc.ID, []byte(doc.RawDocument))
	if e == nil {
		doc.Revision = ""
		doc.Load()
		return true
	}
	log.Printf("Unable to perform raw/internal save: %s", e)
	return false
}

// Save stores the document based on raw data; this will overwrite the RawDocument
// and update _id and _rev on success.  If _id and/or _rev or invalid they will be
// added, updated, or removed (as appropriate).
func (doc *Document) Save(b []byte) (err error) {

	changed := false
	var docID string
	var m map[string]json.Token
	err = json.Unmarshal(b, &m)
	if err != nil {
		return fmt.Errorf("invalid unmarshall when checking byte data: %s", err)
	}

	// Unmarshall _id it out
	if v, ok := m["_id"]; ok {
		docID = v.(string)
	} else {
		if doc.ID == "" {
			uuid, err := doc.database.server.GetUUID()
			if err != nil {
				return fmt.Errorf("document does not have an id assigned or in binary data and attempts to get a uuid have failed: %s", err)
			}
			m["_id"] = uuid // store
			docID = uuid
			changed = true
		}
	}

	// Check for a valid _rev
	if rev, ok := m["_rev"]; ok {
		if rev.(string) == "" {
			delete(m, "_rev")
			changed = true
		} else {
			if doc.Revision != "" {
				m["_rev"] = doc.Revision
				changed = true
			}
		}
	}

	// remarshal if needed
	if changed {
		b, err = json.Marshal(m)
		if err != nil {
			return fmt.Errorf("failed to remarshal data (document): %s", err)
		}
	}

	// use internal save
	err = doc.database.save(docID, b)
	if err != nil {
		return err
	}

	// Update
	if doc.ID == "" {
		doc.ID = docID
	}
	doc.Revision = ""
	doc.Load()

	return nil
}
