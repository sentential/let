// Copyright (c) 2015 Sentential Pty Ltd
// All rights reserved.
//
// Released under zlib/png licence (see LICENCE)

package let

import (
	"encoding/json"
	"log"
)

// SessionUserContext represents the userCtx information from _session.
type SessionUserContext struct {
	Name  string
	Roles []string
}

// Session represents CouchDB session information from _session.
type Session struct {
	Ok       bool
	UserData SessionUserContext `json:"userCtx"`
}

// GetSession gets the current user's session information (or nil and an error if the user is not authenticated).
func (server *Server) GetSession() (ses *Session, err error) {
	status, _, body, err := server.HTTPGet("/_session")
	if err != nil || status != 200 {
		ses = nil
		return
	}

	// Create it
	ses = &Session{}

	// Decode it
	err = json.Unmarshal([]byte(body), ses)
	if err != nil {
		log.Println("Unable to decode session.")
	}
	return
}

// IsAuthenticated is a trivial GetSession wrapper to check for an authenticated state against _session.
func (server *Server) IsAuthenticated() bool {
	ses, err := server.GetSession()
	if err != nil {
		return false
	}
	return ses.Ok
}
