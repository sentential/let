# Frequently Asked Questions

### Why don't you just use `omitempty` on Document's `_rev` and `_id` fields?

For purely internal work that would be fine; for what's actually being done in other things (e.g. in client applications) there's no certainty that `omitempty` will be set.  (They are in Sentential's external applications, but that doesn't help other people!)

