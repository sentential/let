// Copyright (c) 2015 Sentential Pty Ltd
// All rights reserved.
//
// Released under zlib/png licence (see LICENCE)

package let

import (
	"encoding/json"
)

// DesignDocumentView represents a view function from a design document.
type DesignDocumentView struct {
	dd     *DesignDocument `json:"-"` // Don't serialise, users have no reason to use this.
	Name   string          `json:"-"` // Name
	Map    string          `json:"map"`
	Reduce string          `json:"reduce"`
}

// DesignDocumentViewRow represents a row of data within a view.
type DesignDocumentViewRow struct {
	ID    string     `json:"id"`
	Key   json.Token `json:"key"`
	Value json.Token `json:"value"`
}

// ---------------------------------------------------------------------
// DesignDocumentView
// ---------------------------------------------------------------------

// Save triggers a design document save (which should encompass the view)
func (dv *DesignDocumentView) Save() (err error) {
	return dv.dd.Save() // trigger a save
}

// String performs stringification for sprintf (etc.)
func (dv *DesignDocumentView) String() string {

	b, err := json.Marshal(dv)
	if err != nil {
		return "[error stringifying view]"
	}
	return string(b)
}

// Get returns the first page using default options (no skip, no key, no limit)
func (dv *DesignDocumentView) Get() (*ViewPagination, error) {
	vp := &ViewPagination{
		view: dv,
		Query: &PaginationQuery{
			Key:  "",
			Skip: 0,
		},
	}
	err := vp.Load()
	return vp, err
}

// Query uses a Query object (`PaginationQuery`) to perform a guided query.
func (dv *DesignDocumentView) Query(query *PaginationQuery) (*ViewPagination, error) {
	vp := &ViewPagination{
		view:  dv,
		Query: query,
	}
	err := vp.Load()
	return vp, err
}
