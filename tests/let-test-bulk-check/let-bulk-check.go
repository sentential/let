package main

import (
	"bitbucket.org/sentential/let"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
)

func Usage() {
	fmt.Println("Usage:")
	fmt.Printf("%s --uri <uri>", os.Args[0])
	fmt.Println("  --username <username>")
	fmt.Println("  --password <password>")
	fmt.Println("  --db <db>")
	fmt.Println("  --cert <cert>")
	fmt.Println("  --key <key>")
	fmt.Println("  --ca <caFile>")
}

func main() {

	// Use stdout, but keep log live for the main package.
	log.SetOutput(os.Stdout)

	var username *string = flag.String(
		"username",
		"",
		"Username")

	var password *string = flag.String(
		"password",
		"",
		"Password for the given username")

	var uri *string = flag.String(
		"uri",
		"http://127.0.0.1:5984",
		"Host (e.g. http://127.0.0.1:5984 or https://my_proxy_virtualhosting_magic/some_prefix)")

	var dbName *string = flag.String(
		"db",
		"twiggly",
		"Database to get information about.")

	var certFile *string = flag.String(
		"cert",
		"",
		"Path to certificate (for SSL); blank will load non-SSL.")

	var keyFile *string = flag.String(
		"key",
		"",
		"Path to key file (for SSL); blank will load non-SSL.")

	var caFile *string = flag.String(
		"ca",
		"",
		"Path to key file (for SSL); blank will load non-SSL.")

	flag.Parse()

	// Create a server
	server, err := let.ConnectTLS(*uri, *certFile, *keyFile, *caFile)

	// Authenticate
	server.SetCookieAuth(*username, *password)

	// Get the session information; it should report a 'null' username.

	if !server.IsAuthenticated() {
		fmt.Println("Not authenticated; cannot continue.")
		return
	}

	session, err := server.GetSession()
	fmt.Printf("Username: '%s'\n", session.UserData.Name)

	// Gets the database
	db, err := server.GetOrCreate(*dbName)

	// Inform (with any luck there's no error; if there is, something funny is happening or the database doesn't exist)
	if err != nil {
		log.Fatalf("Error getting database ('%s'):\n%s\n", *dbName, err)
		return // fatal
	}

	// The name is exposed (compare to expected)
	fmt.Printf("Database name: '%s' (expected '%s')\n", db.Name, *dbName)

	uuids, err := server.GetUUIDs(50)
	if err != nil {
		log.Fatal("Unable to continue, failed to get UUIDs")
	}

	fmt.Println("Got UUIDs, generating empty/boring documents...")

	bulk := db.GetBulkDocumentObject()

	for _, v := range uuids {
		bulk.Documents = append(bulk.Documents, map[string]json.Token{"_id": v})
	}

	bulk.Save()

}
