package main

import (
	"bitbucket.org/sentential/let"
	"flag"
	"fmt"
	"log"
	"os"
)

func Usage() {
	fmt.Println("Usage:")
	fmt.Printf("%s --uri <uri>", os.Args[0])
	fmt.Println("  --username <username>")
	fmt.Println("  --password <password>")
	fmt.Println("  --db <db>")
	fmt.Println("  --doc <doc>")
	fmt.Println("  --cert <cert>")
	fmt.Println("  --key <key>")
	fmt.Println("  --ca <caFile>")
}

func main() {

	// Use stdout, but keep log live for the main package.
	log.SetOutput(os.Stdout)

	var username *string = flag.String(
		"username",
		"",
		"Username")

	var password *string = flag.String(
		"password",
		"",
		"Password for the given username")

	var uri *string = flag.String(
		"uri",
		"http://127.0.0.1:5984",
		"Host (e.g. http://127.0.0.1:5984 or https://my_proxy_virtualhosting_magic/some_prefix)")

	var dbName *string = flag.String(
		"db",
		"twiggly",
		"Database to get information about.")

	var documentName *string = flag.String(
		"doc",
		"twiggly",
		"Document name (to retrieve from the database)")

	var certFile *string = flag.String(
		"cert",
		"",
		"Path to certificate (for SSL); blank will load non-SSL.")

	var keyFile *string = flag.String(
		"key",
		"",
		"Path to key file (for SSL); blank will load non-SSL.")

	var caFile *string = flag.String(
		"ca",
		"",
		"Path to key file (for SSL); blank will load non-SSL.")

	flag.Parse()

	// Create a server
	server, err := let.ConnectTLS(*uri, *certFile, *keyFile, *caFile)

	// Authenticate
	server.SetCookieAuth(*username, *password)

	// Get the session information; it should report a 'null' username.
	session, err := server.GetSession()

	// Inform (with any luck there's no error; if there is, something funny is happening)
	if err != nil {
		fmt.Printf("Error getting session: %s\n", err)
	} else {
		fmt.Printf("Username: '%s'\n", session.UserData.Name)
	}

	// Test server information
	info, err := server.Info()
	if err == nil {
		fmt.Printf("CouchDB Version: %s\n", info.Version)
	}

	// Get the active tasks (typically it will be 0 unless you're doing something)
	tasks, err := server.GetActiveTasks()
	if err == nil {
		fmt.Printf("Got tasks (%d in progress)\n", len(tasks))
	}

	// Gets the database
	db, err := server.Get(*dbName)

	// Inform (with any luck there's no error; if there is, something funny is happening or the database doesn't exist)
	if err != nil {
		fmt.Printf("Error getting database ('%s'):\n%s\n", *dbName, err)
	} else {
		// The name is exposed (compare to expected)
		fmt.Printf("Database name: '%s' (expected '%s')\n", db.Info.Name, *dbName)

		doc, err := db.Get(*documentName, "")
		if err == nil {
			fmt.Printf("Document loaded: '%s' (expected '%s')\n", doc.ID, *documentName)
			revs, err := doc.GetRevisions()
			if err == nil {
				fmt.Printf("Getting all revisions (order is irrelevant here)...\n")
				for k, v := range revs {
					fmt.Printf("\t%s - '%s'\n", k, v)
				}
				fmt.Printf("\n")
			}
		} else {
			fmt.Printf("Document NOT loaded: %s\nError: %s\n", doc.ID, err)
		}
	}

	// For good measure, get the DB list
	fmt.Printf("\nListing databases (numbers are purely for clarity):\n")
	dbNames, err := server.GetAllDatabases()
	if err == nil {
		for k, v := range dbNames {
			fmt.Printf("\t% 2d. '%s'\n", k, v)
		}
		fmt.Printf("\n")
	}

	// Test events - should fail
	_, err = server.GetAllDatabaseUpdates()
	if err != nil {
		fmt.Printf("Error getting database events:\n %s\n", err)
	}

	// Test membership - should fail
	membership, err := server.GetMembership()
	if err == nil {
		fmt.Printf("Got memberships\n")
		for k, v := range membership {
			fmt.Printf("\tMembership set %s:\n", k)
			for k_, v_ := range v {
				fmt.Printf("\t\t% 2d. %s\n", k_, v_)
			}
			fmt.Printf("\n")
		}
	} else {
		fmt.Printf("Error getting membership:\n%s\n", err)
	}

}
