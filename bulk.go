// Copyright (c) 2015 Sentential Pty Ltd
// All rights reserved.
//
// Released under zlib/png licence (see LICENCE)

package let

import (
	"bytes"
	"encoding/json"
	"fmt"
)

// BulkDocuments represents a collection of documents to be saved, updated, or added, in bulk.
// Contrary to the Save functions in let, BulkDocuments functions have no default safety on the Save function.
type BulkDocuments struct {
	Database     *Database               `json:"-"`
	AllOrNothing bool                    `json:"all_or_nothing"`
	NewEdits     bool                    `json:"new_edits"`
	Documents    []map[string]json.Token `json:"docs"`
}

// Save triggers a bulk save function sent to _bulk_docs.
func (bd *BulkDocuments) Save() error {

	jsonBytes, err := json.Marshal(bd)
	if err != nil {
		return err
	}

	// Fire off the request, hoping it works
	status, _, _, err := bd.Database.server.HTTPPost(fmt.Sprintf("%s/_bulk_docs", bd.Database.Name), bytes.NewReader(jsonBytes))

	if status != 201 {
		return fmt.Errorf("bad status (%d) encountered while bulk saving", status)
	}

	if err != nil {
		return err
	}

	return nil
}
