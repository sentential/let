// Copyright (c) 2015 Sentential Pty Ltd
// All rights reserved.
//
// Released under zlib/png licence (see LICENCE)

package let

import (
	"encoding/json"
	"fmt"
)

// AllDocumentsPagination is an object used for pagination of _all_docs.
type AllDocumentsPagination struct {
	Pagination     // Interface
	PaginationJSON // JSON Variables used by pagination views

	db    *Database        `json:"-"` // database
	Query *PaginationQuery `json:"-"` // Variables
}

// ---------------------------------------------------------------------
// AllDocumentsPagination
// ---------------------------------------------------------------------

// Load retrieves data based on the current settings within the view.
func (ad *AllDocumentsPagination) Load() error {

	path := fmt.Sprintf("%s/_all_docs%s", ad.db.Name, ad.Query.String())

	// Internal server call (respecting TLS)
	status, _, body, err := ad.db.server.HTTPGet(path)

	if status != 200 {
		return fmt.Errorf("bad request received in _all_docs pagination: %d", status)
	}

	// Non-status related error
	if err != nil {
		return err
	}

	// Catch decode related issues.
	err = json.Unmarshal([]byte(body), ad)
	if err != nil {
		return err
	}

	return err
}

// Next performs a Load after moving skip forward based on the limit.
// If the limit is 0 nothing happens.
func (ad *AllDocumentsPagination) Next() error {
	if ad.Query.Limit != 0 {
		ad.Query.Skip = ad.Skip + ad.Query.Limit
		return ad.Load()
	}
	return nil // nothing happened
}

// Previous performs a Load after moving skip back based on the limit.
// Where limit is 0 but skip is not the query just sets skip to 0 and loads.
func (ad *AllDocumentsPagination) Previous() error {
	if ad.Query.Skip != 0 { // if skipped, reset the skip.
		if ad.Query.Limit == 0 {
			ad.Query.Skip = 0 // clear the skip
		} else {
			ad.Query.Skip = ad.Skip - ad.Query.Limit
			if ad.Query.Skip < 0 {
				ad.Query.Skip = 0
			}
		}
		return ad.Load()
	}
	return nil // nothing happened
}
