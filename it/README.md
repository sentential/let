# Let It (Go) - `_changes` related sub-project

More details will appear as this project is brought to life; this folder stands as a reminder that it is something on the table (albeit a lower priority than other projects of Sentential).

## Dependencies 

* `it` will depend on `let` (being a subproject).
* All efforts will be made to keep it to `let`.

## Relevant API documentation

Check out CouchDB's documentation, particularly the information pertaining to the [_changes](http://docs.couchdb.org/en/latest/api/database/changes.html) API.