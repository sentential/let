// Copyright (c) 2015 Sentential Pty Ltd
// All rights reserved.
//
// Released under zlib/png licence (see LICENCE)

package let

import (
	"encoding/json"
	"fmt"
)

// ViewPagination is an object used for pagination of a view.
type ViewPagination struct {
	Pagination                         // Interface
	Query          *PaginationQuery    // Variables
	view           *DesignDocumentView // View
	PaginationJSON                     // JSON Variables used by pagination views
}

// ---------------------------------------------------------------------
// ViewPagination
// ---------------------------------------------------------------------

// Load retrieves data based on the current settings within the view.
func (vp *ViewPagination) Load() error {

	path := fmt.Sprintf("%s/_view/%s/%s",
		vp.view.dd.Path,
		vp.view.Name,
		vp.Query.String(),
	)

	// Internal server call (respecting TLS)
	status, _, body, err := vp.view.dd.db.server.HTTPGet(path)

	// Catch any bad status call based on the docs;
	// Following the documentation anything non-200 is a huge problem.
	if status != 200 {
		var errorString string
		switch status {
		// case 200 // OK - ignore
		case 400:
			errorString = "400 bad request"
			break
		case 401:
			errorString = "401 read refused"
			break
		case 404:
			errorString = "404 not found"
			break
		case 500:
			errorString = "500 internal server error"
			break
		default:
			errorString = fmt.Sprintf("unknown error (code: %d)", status)
			break
		}
		return fmt.Errorf("bad request received in viewpagination: %s", errorString)
	}

	// Non-status related error
	if err != nil {
		return err
	}

	// Catch decode related issues.
	err = json.Unmarshal([]byte(body), vp)
	if err != nil {
		return err
	}

	return err
}

// Next performs a Load after moving skip forward based on the limit.
// If the limit is 0 nothing happens.
func (vp *ViewPagination) Next() error {
	if vp.Query.Limit != 0 {
		vp.Query.Skip = vp.Skip + vp.Query.Limit
		return vp.Load()
	}
	return nil // nothing happened
}

// Previous performs a Load after moving skip back based on the limit.
// Where limit is 0 but skip is not the query just sets skip to 0 and loads.
func (vp *ViewPagination) Previous() error {
	if vp.Query.Skip != 0 { // if skipped, reset the skip.
		if vp.Query.Limit == 0 {
			vp.Query.Skip = 0 // clear the skip
		} else {
			vp.Query.Skip = vp.Skip - vp.Query.Limit
			if vp.Query.Skip < 0 {
				vp.Query.Skip = 0
			}
		}
		return vp.Load()
	}
	return nil // nothing happened
}
